# Description
Comparison of model parameters for ATLAS and CMS

# Responsibles
- Sam Meehan <samuel.meehan@cern.ch>
- Shin-Shan Yu <syu@cern.ch>

# Contents
- *ATLAS Cross Sections* : GatheredCrossSections_pdf263400_mFixedToMA.txt 
- *CMS Cross Sections* : CMS_sigmas_20170512.txt
- *Comparison Script* : CompareCMSATLAS.py

# Usage
Get the repo and run the comparison script
```
git clone https://meehan@gitlab.cern.ch/meehan/Zp2HDM_ParameterChoice_CMSATLAS.git
cd Zp2HDM_ParameterChoice_CMSATLAS
python CompareCMSATLAS.py
```
*NOTE* that the syntax for the python script will break in python 3 (it seems)
