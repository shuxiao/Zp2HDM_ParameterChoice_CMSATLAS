# LHAID    mzp    ma    mhp    mh2    mDM    tanBeta    gZ    Sigma[pb]

Sigma_CMS={}
infile = open("CMS_sigmas_20170527.txt","r")
lines = infile.readlines()

for line in lines: 
    print line
    
    if "LHA" in line:
        continue
    
    pdf   = int(line.split()[0])
    mz    = str(int(float(line.split()[1])))
    ma    = str(int(float(line.split()[2])))
    mhp   = int(float(line.split()[3]))
    mh2   = int(float(line.split()[4]))
    sigma = float(line.split()[8])

    print "CMS : ",mz,ma,sigma
    
    Sigma_CMS[mz+","+ma]=sigma
        
       
       
       
Sigma_ATLAS={}
       
infile = open("GatheredCrossSections_pdf263400_mFixedToMA.txt","r")
lines = infile.readlines()

for line in lines: 
    print line
    
    if "#" in line:
        continue
    
    pdf   = int(line.split()[0])
    mz    = str(int(line.split()[1]))
    ma    = str(int(line.split()[2]))
    mhp   = int(line.split()[3])
    mh2   = int(line.split()[4])
    sigma = float(line.split()[8])
    
    Sigma_ATLAS[mz+","+ma]=sigma

infile.close()




threshold = 0.005
vals_mz = [400,600,800,1000,1200,1400,1600,1800,2000,2200,2400,2600,2800,3000]
vals_ma = [300,400,500,600,700,800,900,1000,1100]

outcheck = open("Zp2HDM_CrossSectionCheck_ComparisonOutput20170527.txt","w+")
outcheck.write("Comparing with difference threshold of : "+str(threshold)+"\n")


for mz in vals_mz:
    for ma in vals_ma:

        mz  = str(mz)
        ma  = str(ma)
        key = mz+","+ma

        sigma_cms   = -1
        sigma_atlas = -1

        if key in Sigma_ATLAS.keys():
            sigma_atlas = Sigma_ATLAS[key]
            
        if key in Sigma_CMS.keys():
            sigma_cms = Sigma_CMS[key]

        lineprint=""
        if sigma_atlas==-1 or sigma_cms==-1:
            lineprint = "X - Someone is missing this mass point"
        else:   
            ratio = sigma_cms/sigma_atlas
            fracDiff = ratio-1.0
            lineprint = "********************* : FracDiff=",fracDiff
            if fracDiff<threshold:
                lineprint = "SAME(good)"

        lineout = "mz={0:6}    ma={1:6}    ATLAS={2:20}    CMS={3:20}    {4}".format(mz,ma,sigma_atlas,sigma_cms,lineprint)
        
        print lineout
        outcheck.write(lineout+"\n")
        
outcheck.close()


